import React from 'react';
import { useCounter } from './useCounter';

//Componente que apresenta uma view para a lógica do useCounter
export function Display(props) {
    const { count, increment, decrement } = useCounter(props.start);

    return (
        <div>
            <button onClick={increment}> Increase </button>
            <button onClick={decrement}> Decrease </button>
            <h1>{count}</h1>
        </div>
    );
};
