import React from 'react';
import { useCounter } from './useCounter';

//Componente que apresenta uma outra view para a lógica do useCounter
export function FancyDisplay(props) {
    const { count, increment, decrement } = useCounter(props.start);

    return (
        <section>
            <button onClick={increment}> Increase </button>
            <button onClick={decrement}> Decrease </button>
            <h2 style='red'>{count}</h2>
        </section>
    );
};
