import { useState } from 'react';

//Componente encapsulando a lógica do counter
export function useCounter(startingCounter) {
    const [count, setCount] = useState(startingCounter);

    const increment = () => setCount(count + 1);
    const decrement = () => setCount(count - 1);

    return {
        count,
        increment,
        decrement,
    };
};
