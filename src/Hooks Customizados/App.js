import React from 'react';
import { Display } from './Display';
import { FancyDisplay } from './FancyDisplay';

function App(){
    return (
        <div>
            <Display start={20}/>
            <FancyDisplay start={10}/>
        </div>
    );
}

export default App;