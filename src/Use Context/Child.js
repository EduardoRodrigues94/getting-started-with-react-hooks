import React from 'react';
import { GrandChild } from './GrandChild';

export function Child(){
    return <GrandChild />;
};