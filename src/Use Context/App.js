import React, { useState, createContext } from 'react';
import { Child } from './Child';

//o context é utilizado para que dentro de uma árvore de componentes
//o componente filho consiga receber a informação que deseja
//sem que a informação tenha que ser repassada a todos os componentes
//anteriores na árvore
export const NameContext = createContext();

function App(){
    const [name, setName] = useState('Eduardo Rodrigues');

    return(
      <NameContext.Provider value={name}>
          <Child />
      </NameContext.Provider>
    );
}

export default App;
