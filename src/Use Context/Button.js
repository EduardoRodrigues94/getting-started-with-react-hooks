import React, { useContext } from 'react';
import { NameContext } from './App';

export function Button(){
    //Importa o contexto do App e o utiliza para pegar o Name
    const name = useContext(NameContext);

    return <button>{name}</button>;
};