import React from 'react';
import ReactDOM from 'react-dom';
import App from './Use Effect on Events/App';

ReactDOM.render(<App />, document.getElementById('root'));