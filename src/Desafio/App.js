import React from 'react';
import { useInput } from './useInput';

function App(){
    return(
        <form>
            <input
                type="text"
                placeholder="name"
                // Esse tipo de abordagem abaixo só se vale por que input tem atributos com nomes value e onChange
                // Do contrário seria necessário criar desconstrutores para o retorno da função useInput()
                // const {value: name, onChange: handleNameChange} = useInput();
                // e atribuílos aqui aos atributos
                // value={name}
                // onChange={handleNameChange}
                { ...useInput() } 
            />
            <input
                type="text"
                placeholder="surname"
                { ...useInput() }
            />
            <input
                type="number"
                placeholder = "Age"
                { ...useInput() }
            />
        </form>
    );
}

export default App;