import React from 'react';
import { useKeyPress } from './useKeyPress';

function App(){
    const userText = useKeyPress("Once upon a time...");
    
    return(
        <div>
            <h1>Feel free to type your text will show up below!</h1>
            <blockquote>
                { userText }
            </blockquote>
        </div>
    )
}

export default App;