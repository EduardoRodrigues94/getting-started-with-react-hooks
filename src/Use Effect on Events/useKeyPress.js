import { useState, useEffect } from 'react';

//Custom hook acrescentando/removendo um evento ao pressionar das teclas
export function useKeyPress(startingValue) {
    const [userText, setUserText] = useState(startingValue);

    const handleUserKeyPress = event => {
        const { key, keyCode } = event;

        if ((keyCode === 32) || (keyCode >= 65 && keyCode <= 90)) {
            setUserText(`${userText}${key}`);
        }
    };

    //useEffect passa uma função que é chamada sempre que o componente é renderizado
    useEffect(() => {
        window.addEventListener('keydown', handleUserKeyPress);

        //a função de rotorno entra em uma Queue e acontece antes do próximo evento acontecer
        return (() => {
            window.removeEventListener('keydown', handleUserKeyPress);
        });
    });

    //retornando o userText para que seja utilizado no componente pai
    return userText;
};