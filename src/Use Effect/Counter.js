import React, { useState, useEffect } from 'react';

export function Counter(){
    const [count, setCount] = useState(0);
    const [color, setColor] = useState('salmon');

    const handleIncrease = () => setCount(count + 1);
    const handleDecrease = () => setCount(count - 1);

    useEffect(() => {
        console.log(`I'm inside the useEffect function. \nThe current count is ${count}`);
        
        //Entra numa Queue e roda antes que o próximo efeito rode quando a segundo parâmetro da função mudar de valor
        return () => {
            console.log(`I'm removing anything that was setup above. onComponentWillUnmount the last count was ${count}`);
        };
    }, [count]);

    const handleColorChange = () => {
        const nextColor = color === 'salmon' ? 'gold' : 'salmon';

        setColor(nextColor);
    };

    return (
        <div>
            <button onClick={handleIncrease}> Increase </button>
            <button onClick={handleColorChange}> Change Color </button>
            <button onClick={handleDecrease}> Decrease </button>

            <h1 style={{color}}>{ count }</h1>
        </div>
    );
};
