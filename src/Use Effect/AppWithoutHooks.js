import React from 'react';

//Exemplo seguindo sintaxe antiga para fazer um simples counter
class Counter extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            count: 0,
        };
    }
    componentDidMount() {
        console.log("this is componentDidMount. After first render");
    }
    
    componentDidUpdate(prevProps, prevState){
        console.log("this is componentDidUpdate. After any subsequent render");
    }
    
    componentWillUnmount(){
        console.log("this is componentWillUnmount. I'm leaving");
    }

    render(){
        console.log('Rendering');
        const {count} = this.state;

        return(
            <div>
                <button onClick={() => this.setState({count: count+1})}>Increase</button>
                <button onClick={() => this.setState({count: count-1})}>Decrease</button>
                <button onClick={() => this.setState({count: 0})}>Reset</button>
                <h1>{count}</h1>
            </div>
        )
    }
}

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            visible: true,
        };
    }

    render(){
        const {visible} = this.state;
        return(
            <div>
                <button onClick={() => this.setState({visible: !visible})}>
                    Show/Hide the counter component
                </button>
                {visible && <Counter/>}
            </div>
        )
    }
}

export default App;