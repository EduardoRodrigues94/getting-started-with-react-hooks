import React, { useState } from 'react';
import { Counter } from './Counter';

function App(){
    const [visible, setVisible] = useState(false);
    
    return(
        <div>
            <button onClick={() => setVisible(!visible)}>Show Hide/Counter Component</button>
            
            { visible && <Counter/> }
        </div>
    );
}

export default App;